#!/usr/bin/env python

def insertion_sort(arr):
    for j in range(1, len(arr)):
        key = arr[j]
        i = j - 1
        while i >= 0 and arr[i] > key:
            arr[i + 1] = arr[i]
            i = i - 1
        arr[i + 1] = key

# Exercise 2.1-2
def insertion_sort2(arr):
    for j in range(1, len(arr)):
        key = arr[j]
        i = j - 1
        while i >= 0 and arr[i] < key:
            arr[i + 1] = arr[i]
            i = i - 1
        arr[i + 1] = key

# Exercise 2.1-4
def sum_bit_arrays(a, b, n):
    c = [0 for x in range(n + 1)]
    for i in reversed(range(n)):
        c[i + 1] += a[i] + b[i]
        c[i] += c[i + 1] // 2
        c[i + 1] %= 2
    return c

def main():
    arr = [5, 2, 4, 6, 1, 3]
    print(arr)
    insertion_sort2(arr)
    print(arr)
    print(sum_bit_arrays([1, 0, 1, 1], [1, 1, 1, 1], 4))

if __name__ == "__main__":
    main()
